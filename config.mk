# Automatically generated file. DO NOT MODIFY

$(call inherit-product, packages/apps/MotoWavesV2/motowavesv2.mk)

# Properties
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.audio_fx.current=waves \
    persist.vendor.audio_fx.waves.maxxsense=true \
    persist.vendor.audio_fx.waves.processing=true \
    persist.vendor.audio_fx.waves.proc_twks=true \
    persist.vendor.audio_fx.waves.systrace=true

# Sepolicy
BOARD_VENDOR_SEPOLICY_DIRS += \
    packages/apps/MotoWavesV2/sepolicy/vendor
