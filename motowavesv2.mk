# Automatically generated file. DO NOT MODIFY

PRODUCT_SOONG_NAMESPACES += \
    packages/apps/MotoWavesV2

PRODUCT_COPY_FILES += \
    packages/apps/MotoWavesV2/proprietary/vendor/lib/soundfx/libmaxxeffect-cembedded.so:$(TARGET_COPY_OUT_VENDOR)/lib/soundfx/libmaxxeffect-cembedded.so

PRODUCT_PACKAGES += \
    MotoWavesV2
